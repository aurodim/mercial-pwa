import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { FeedComponent } from './feed/feed.component';
import { OffersComponent } from './offers/offers.component';
import { SettingsComponent } from './settings/settings.component';
import { APIService } from './auth/api.service';
import { AuthService } from './auth/auth.service';
import { FeedService } from './feed/feed.service';
import { ToolbarService } from './toolbar.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MzCollectionModule, MzToastModule, MzInputModule, MzSelectModule,
   MzButtonModule, MzModalModule, MzRadioButtonModule, MzCheckboxModule } from 'ngx-materialize';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HttpClientModule } from '@angular/common/http';
import { MercialComponent } from './feed/mercial/mercial.component';
import { ViewerComponent } from './feed/viewer/viewer.component';
import { MercialViewerService } from './feed/viewer/viewer.mercial.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { OfferService } from './offers/offers.service';
import { OfferViewerService } from './offers/redeem/viewer.offer.service';
import { OfferComponent } from './offers/offer/offer.component';
import { RedeemComponent } from './offers/redeem/redeem.component';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { RedeemValidatorComponent } from './validator/redeem-validator.component';
import { MzProgressModule } from 'ngx-materialize';
import { MzTooltipModule } from 'ngx-materialize';
import { MzDatepickerModule } from 'ngx-materialize';

const config: SocketIoConfig = { url: environment.endpoint, options: {autoConnect: false} };

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    AnalyticsComponent,
    FeedComponent,
    OffersComponent,
    MercialComponent,
    OfferComponent,
    RedeemComponent,
    ViewerComponent,
    SettingsComponent,
    RedeemValidatorComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    BrowserAnimationsModule,
    InfiniteScrollModule,
    MzModalModule,
    MzButtonModule,
    MzSelectModule,
    MzInputModule,
    MzToastModule,
    MzCollectionModule,
    MzRadioButtonModule,
    MzCheckboxModule,
    MzProgressModule,
    MzTooltipModule,
    MzDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(config),
    HttpClientModule,
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'aurodim' } as CloudinaryConfiguration),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [APIService, AuthService, FeedService, OfferService, ToolbarService, MercialViewerService, OfferViewerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
