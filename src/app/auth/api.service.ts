import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { MzToastService } from 'ngx-materialize';

@Injectable()
export class APIService {
  public api: string;

  constructor(private http: HttpClient, private authService: AuthService, private toastService: MzToastService) {
    this.api = environment.endpoint;
  }

  AUTH() {
    return this.authService.getAUTH();
  }

  // GET
  analyticsData(onSuccess: (response: any) => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/analytics', { params : { auth : this.authService.getAUTH()}, responseType : 'json' })
    .subscribe((response: any) => onSuccess(response), () => onError());
  }

  feedData(previousPopulations = 0, filters: any, onSuccess: (response: any) => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/feed', { params : { auth : this.authService.getAUTH(), prevPop : previousPopulations.toString(),
     date : filters.date, meta : filters.meta, categories : filters.categories, lang : 'en'},
      responseType : 'json' }).subscribe((response: any) => onSuccess(response), () => onError());
  }

  feedItem(id: string, onSuccess: () => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/feed' + id,
    { params : { auth: this.authService.getAUTH(), lang: 'en'}, responseType : 'json' }).subscribe(
      () => onSuccess(), () => onError());
  }

  offersData(previousPopulations = 0, filters: any, onSuccess: (response: any) => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/offers', { params : { auth : this.authService.getAUTH(),
       prevPop : previousPopulations.toString(), date : filters.date, meta : filters.meta,
        location : filters.location, lang : 'en'}, responseType : 'json' })
    .subscribe((response: any) => onSuccess(response), () => onError());
  }

  offerItem(id: string, onSuccess: (response: any) => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/offers/' + id, { params : { auth : this.authService.getAUTH(),
       lang : 'en'}, responseType : 'json' })
    .subscribe((response: any) => onSuccess(response), () => onError());
  }

  reviewsData(previousPopulations = 0, onSuccess: (response: any) => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/reviews', { params : { auth : this.authService.getAUTH(), prevPop : previousPopulations.toString(),
       lang : 'en'}, responseType : 'json' })
    .subscribe((response: any) => onSuccess(response), () => onError());
  }

  fastQR(values: any, onSuccess: (response: any) => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/offers/' + values.id + '/fqr', { params : { auth : this.authService.getAUTH(),
       offer : values.id}, responseType : 'text' })
    .subscribe((response: any) => onSuccess(response), () => onError());
  }

  settingsData(onSuccess: (response: any) => void, onError?: () => void) {
    this.http.get(this.api + '/api/user/settings', { params : { auth : this.authService.getAUTH()}, responseType : 'json' })
    .subscribe((response: any) => onSuccess(response), () => onError());
  }

  // POST
  registerUser(requirements: object, onSuccess: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(requirements);
    this.http.post(this.api + '/api/user/register', body, { headers }).subscribe(
      (response: any) => onSuccess(response), (error) => this.serverError(error));
  }

  loginUser(credentials: object, onSuccess: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    this.http.post(this.api + '/api/user/login', body, { headers }).subscribe(
      (response: any) => onSuccess(response), (error) => this.serverError(error));
  }

  forgot(credentials: object, onSuccess: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    this.http.post(this.api + '/api/user/forgot', body, { headers }).subscribe(
      (response: any) => onSuccess(response), (error) => this.serverError(error));
  }

  reset(credentials: object, onSuccess: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + '/api/user/reset', body, { headers }).subscribe(
      (response: any) => onSuccess(response), (error) => this.serverError(error));
  }

  mercialWatched(values: any, onSuccess: (response: any) => void, onError?: () => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    this.http.post(this.api + `/api/user/feed/${values.id}/watched`, body, {headers}).subscribe(
      (response: any) => onSuccess(response), () => onError());
  }

  report(values: any, onSuccess: (response: any) => void, onError?: () => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    this.http.post(this.api + `/api/user/report`, body, {headers}).subscribe(
      (response: any) => onSuccess(response), () => onError());
  }

  addInterest(values: any) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    return this.http.post(this.api + `/api/user/feed/${values.id}/interest`, body, {headers}).subscribe(() => {});
  }

  newPurchase(values: any, onSuccess?: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    this.http.post(this.api + `/api/user/purchases/new`, body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  validateRedeem(values: any, onSuccess?: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    this.http.post(this.api + `/api/user/offers/validate`, body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  rateFranchise(values: any, onSuccess?: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    this.http.post(this.api + `/api/user/rate`, body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  enterZipcode(values: any, onSuccess?: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    this.http.post(this.api + `/api/user/zes`, body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  reviewAnswered(values: any) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body: any = JSON.stringify(values);
    this.http.post(this.api + `/api/user/reviews/${values.id}/answered`, body, { headers }).subscribe(() => {});
  }

  updateAccount(values: object, onSuccess: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    this.http.post(this.api + '/api/user/settings/account', body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  updateTarget(values: object, onSuccess: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    this.http.post(this.api + '/api/user/settings/target', body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  updateLocation(values: object, onSuccess?: () => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    this.http.post(this.api + '/api/user/settings/location', body, { headers }).subscribe(() => onSuccess());
  }

  updatePreferences(values: object, onSuccess?: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    this.http.post(this.api + '/api/user/settings/prefs', body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  updatePrivacy(values: object, onSuccess?: (response: any) => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    this.http.post(this.api + '/api/user/settings/privacy', body, { headers }).subscribe((response: any) => onSuccess(response));
  }

  deleteAccount(values: object, onSuccess?: () => void) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    this.http.post(this.api + '/api/user/delete', body, { headers }).subscribe(() => onSuccess());
  }

  serverError(error?: { status: any; }) {
    this.toastService.show(`Server Error - ${error.status}`, 3000, 'red');
  }
}
