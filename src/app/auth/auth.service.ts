import { Injectable, EventEmitter } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { Socket } from 'ngx-socket-io';

@Injectable()
export class AuthService {
  private aT: string = null;
  private merchips = 0;
  private fullname: string = null;
  private authenticated = false;

  constructor(private socket: Socket) {
    this.fullname = localStorage.getItem('_fn');
    this.aT = localStorage.getItem('_at');
    this.merchips = parseInt(localStorage.getItem('_m'), 10);

    if (this.fullname != null && this.aT != null && this.merchips != null) {
      this.setAuthenticated(true);
    }
  }

  // SETTERS
  public authenticate(body: any, cb: () => void) {
    this.aT = body._tk;
    this.fullname = body._fn;
    this.merchips = body._c;
    localStorage.setItem('_m', this.merchips.toString());
    localStorage.setItem('_fn', this.fullname);
    localStorage.setItem('_at', this.aT);
    this.setAuthenticated(true);
    cb();
  }

  public setMerchips(merchips: number) {
    this.merchips = merchips;
    localStorage.setItem('_m', this.merchips.toString());
  }

  public logoutUser() {
    return new Promise((resolve) => {
      this.fullname = null;
      this.aT = null;
      this.merchips = 0;
      localStorage.removeItem('_fn');
      localStorage.removeItem('_at');
      localStorage.removeItem('_m');
      this.setAuthenticated(false);
      resolve();
    });
  }

  public setAuthenticated(authenticated: boolean) {
    if (this.authenticated) {
      this.socket.connect();
    } else {
      this.socket.disconnect();
    }
    this.authenticated = authenticated;
  }

  // GETTERS
  public getAUTH() {
    return this.aT;
  }

  public getSocket() {
    const decodedToken = jwt_decode(this.aT);
    return decodedToken._s;
  }

  public isAuthenticated() {
    return this.authenticated;
  }

  public getMerchips(format: boolean = false) {
    if (format) {
      let formatted = '';
      if (this.merchips / 1000000000 >= 1) {
          formatted += (this.merchips / 1000000000).toFixed(2) + 'B';
      } else if (this.merchips / 1000000 >= 1) {
        formatted += (this.merchips / 1000000).toFixed(2) + 'M';
      } else if (this.merchips / 1000 >= 1) {
        formatted += (this.merchips / 1000).toFixed(2) + 'K';
      } else {
        return this.merchips;
      }

      const formattedArr = formatted.split('.');

      return formattedArr[1].substring(0, 2) === '00' ? formattedArr[0] + formattedArr[1].substring(2) : formatted;
    }
    return this.merchips;
  }

  public getFullname() {
    return this.fullname;
  }
}
