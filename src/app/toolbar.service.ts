import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class ToolbarService {
  filterOpen = false;
  filterOffersOpen = false;
  private loading = false;
  private refreshSubject = new Subject<boolean>();
  private filterSubject = new Subject<boolean>();
  private filterOffersSubject = new Subject<boolean>();
  private loadingSubject = new Subject<boolean>();

  triggerRefresh() {
    this.refreshSubject.next();
  }

  triggerFilter() {
    this.filterOpen = !this.filterOpen;
    this.filterSubject.next(this.filterOpen);
  }

  triggerFilterOffers() {
    this.filterOffersOpen = !this.filterOffersOpen;
    this.filterOffersSubject.next(this.filterOffersOpen);
  }

  filterClosed() {
    this.filterOpen = false;
  }

  filterOffersClosed() {
    this.filterOffersOpen = false;
  }

  getRefresh(): Observable<any> {
    return this.refreshSubject.asObservable();
  }

  getFilter(): Observable<any> {
    return this.filterSubject.asObservable();
  }

  getFilterOffers(): Observable<any> {
    return this.filterOffersSubject.asObservable();
  }

  getLoading() {
    return this.loadingSubject.asObservable();
  }

  isLoading() {
    return this.loading;
  }

  load() {
    this.loadingSubject.next(true);
    this.loading = true;
  }

  stopLoading() {
    this.loadingSubject.next(false);
    this.loading = false;
  }
}
