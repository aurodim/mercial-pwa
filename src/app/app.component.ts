import { Component } from '@angular/core';
import { ToolbarService } from './toolbar.service';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mercial-app';
  feedFilters = false;
  offerFilters = false;
  loading = true;
  isIOS = false;
  isStandalone = false;

  constructor(public authService: AuthService, private router: Router, private toolbarService: ToolbarService) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.feedFilters = event.url.includes('feed') || event.url === '/' || event.url === '';
        this.offerFilters = event.url.includes('offers');
      }
    });
    this.toolbarService.getLoading().subscribe(loading => {
      setTimeout(() => this.loading = loading, 50);
    });
    this.isIOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    this.isStandalone = ('standalone' in window.navigator) && ((window.navigator as any).standalone);
  }

  onRefresh() {
    this.toolbarService.triggerRefresh();
  }

  onFilter() {
    this.toolbarService.triggerFilter();
  }

  onFilterOffers() {
    this.toolbarService.triggerFilterOffers();
  }
}
