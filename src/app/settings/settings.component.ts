import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { APIService } from '../auth/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToolbarService } from '../toolbar.service';
import { MzToastService } from 'ngx-materialize';
import { environment } from '../../environments/environment';

declare var Stripe;
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  passwordDisplay = 'password';
  deletionIsValid = false;
  accountEmail: string;
  stripe = Stripe(environment.STRIPE_PUBLIC);
  price = '';
  accountForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    email : new FormControl({value: '', disabled: false}, { validators: [Validators.email, Validators.required], updateOn: 'blur' }),
    password : new FormControl('')
  });
  targetsForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    ethnicity : new FormControl(''),
    languages : new FormControl([]),
    birthdate: new FormControl('')
  });
  locationForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    zipcode : new FormControl(''),
  });
  preferencesForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    tags : new FormControl(''),
  });
  ethnicities = [
    { value : 'asian/pacisl', init : false, formatted : 'Asian / Pacific Islander' },
    { value : 'black/afriame', init : false, formatted : 'Black or African American' },
    { value : 'hispanic/latino', init : false, formatted : 'Hispanic or Latino' },
    { value : 'natamer/ameind', init : false, formatted : 'Native American or American Indian' },
    { value : 'white', init : true, formatted : 'White' },
    { value : 'other', init : false, formatted : 'Other' }
  ];
  legalDocs = [
    {value : 'privacy-policy', formatted: 'Privacy Policy'},
    {value : 'terms-of-use', formatted: 'Terms of Service'},
    {value : 'client-guidelines', formatted: 'Guidelines'},
    {value: 'cookies', formatted: 'Cookies Policy'},
    {value: 'dmca', formatted: 'DMCA'},
    {value : 'disclaimer', formatted: 'Disclaimer'}
  ];
  languages = [
    { value : 'en', formatted : 'English'},
    { value : 'es', formatted : 'Español'},
    { value : 'fr', formatted : 'Français'}
  ];
  // privacyForm = new FormGroup({
  //   auth : new FormControl(this.api.AUTH()),
  //   notif : new FormControl(false)
  // });
  deleteAccountForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    email : new FormControl({value : '', disabled : false}, [Validators.email, Validators.required]),
  });
  maxDate: string;

  constructor(private api: APIService, private router: Router, public authService: AuthService,
              private toolbarService: ToolbarService, private toastService: MzToastService, private route: ActivatedRoute) {
    if (!authService.isAuthenticated()) {
      router.navigate(['/login']);
    }
    this.maxDate = new Date(new Date().getTime() - 4.1e+11).toISOString();
  }

  ngOnInit() {
    this.toolbarService.load();
    this.api.settingsData(
      (response: any) => {
        const res = response._body._d;
        this.accountForm.controls.email.setValue(res._a.email);
        this.locationForm.controls.zipcode.setValue(res._l.zipcode);
        this.preferencesForm.controls.tags.setValue(res._p.tags.join(', '));
        this.targetsForm.controls.ethnicity.setValue(res._t.eth);
        this.targetsForm.controls.languages.setValue(res._t.langs);
        this.targetsForm.controls.birthdate.setValue(res._t.birthdate);

        if (res._t.eth !== '') {
          this.targetsForm.controls.ethnicity.disable();
        }
        if (res._t.birthdate !== '') {
          this.targetsForm.controls.birthdate.disable();
        }
        // this.privacyForm.controls.notif.setValue(res._py.notif);
        this.accountEmail = res._a.email;
        this.toolbarService.stopLoading();
      }, () => {
        this.toolbarService.stopLoading();
      }
    );
    this.route.queryParams.subscribe(params => {
      if (params['payment-success']) {
        this.api.newPurchase({auth: this.authService.getAUTH(), data: {checkoutID : params['payment-success']}},
          (response: any) => {
            const res = response._body;
            if (res._m === 'error') {
              this.router.navigate([], { queryParams: {}});
              return this.toastService.show('Error processing payment', 3000, 'red');
            } else if (res._m === 'exists') {
              this.router.navigate([], { queryParams: {}});
              return this.toastService.show('Payment already handled', 3000, 'red');
            }
            this.authService.setMerchips(res._d._am);
            this.toastService.show('Payment successful', 3000, 'darkgrey');
          }
        );
      }
    });
  }

  onBuyMerchips(sku: string, price: string) {
    if (price === '') {
      return;
    }
    this.price = price;
    this.stripe.redirectToCheckout({
      items: [{sku, quantity: 1}],

      // Do not rely on the redirect to the successUrl for fulfilling
      // purchases, customers may not always reach the success_url after
      // a successful payment.
      // Instead use one of the strategies described in
      // https://stripe.com/docs/payments/checkout/fulfillment
      successUrl: environment.home + '/settings?payment-success={CHECKOUT_SESSION_ID}',
      cancelUrl: environment.home + '/settings',
    }).then((result: { error: { message: string; }; }) => {
      if (result.error) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer.
        this.toastService.show(result.error.message, 3000, 'red');
      }
    });
  }

  togglePassword(hide: boolean) {
    this.passwordDisplay = hide ? 'password' : 'text';
  }

  validateDeletion() {
    const valid = this.deleteAccountForm.value.email.toLowerCase() === this.accountEmail;
    this.deletionIsValid = valid;
  }

  onAccountSubmit() {
    if (this.accountForm.invalid) {
      return;
    }
    this.api.updateAccount(this.accountForm.value,
      (response: any) => {
        const res = response._body;

        if (res._m === '_emailinuse') {
          this.toastService.show('E-mail already in use', 3000, 'red');
          this.accountForm.controls.email.setErrors({inuse : true});
        } else if (res._m === '_passshort') {
          this.toastService.show('Password is too short', 3000, 'red');
          this.accountForm.controls.email.setErrors({tooshort : true});
        } else {
          this.toastService.show('Account updated', 3000, 'darkgrey');
          this.accountForm.reset(this.accountForm.value);
        }
      }
    );
  }

  onTargetSubmit() {
    if (this.targetsForm.invalid) {
      return;
    }
    this.api.updateTarget(this.targetsForm.value,
      (response: any) => {
        if (response._m === '_dobinvalid') {
          this.targetsForm.controls.dob.setErrors({invalid : true});
          this.toastService.show('Invalid date of birth', 3000, 'red');
          return;
        }
        this.toastService.show('Information updated', 3000, 'darkgrey');
      }
    );
  }

  onLocationSubmit() {
    if (this.locationForm.invalid) {
      return;
    }
    this.api.updateLocation(this.locationForm.value,
      () => {
        this.toastService.show('Location updated', 3000, 'darkgrey');
        this.locationForm.reset(this.locationForm.value);
      }
    );
  }

  onPreferencesSubmit() {
    if (this.preferencesForm.invalid) {
      return;
    }
    this.api.updatePreferences(this.preferencesForm.value,
      (response: any) => {
        const res = response._body;
        this.preferencesForm.controls.tags.setValue(res._f.join(', '));
        this.toastService.show('Preferences updated', 3000, 'darkgrey');
        this.preferencesForm.reset(this.preferencesForm.value);
      }
    );
  }

  // onPrivacySubmit() {
  //   if (this.privacyForm.invalid) {
  //     return;
  //   }
  //   this.loaderService.handleLoader(true);
  //   this.api.updatePrivacy(this.privacyForm.value).subscribe(
  //     (response : any) => {
  //       this.snackbarService.handleError({'message' : 'privacy_updated', 'isError' : false})
  //       this.privacyForm.reset(this.privacyForm.value);
  //       this.loaderService.handleLoader(false);
  //     }
  //   );
  // }

  onDeleteSubmit() {
    if (this.deleteAccountForm.invalid) {
      return;
    }
    this.api.deleteAccount(this.deleteAccountForm.value,
      () => {
        this.toastService.show('Account deleted', 3000, 'darkgrey');
        // this.privacyForm.reset(this.deleteAccountForm.value);
        this.onLogout();
      }
    );
  }

  onLogout() {
    this.authService.logoutUser().then(() => {
      this.router.navigate(['/login']);
    });
  }

}
