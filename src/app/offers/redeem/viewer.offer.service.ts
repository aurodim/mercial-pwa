import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class OfferViewerService {
  private subject = new Subject<boolean>();
  private offerShowing = false;

  triggerViewer(show: boolean) {
    this.offerShowing = show;
    this.subject.next(show);
  }

  public getShowing() {
    return this.offerShowing;
  }

  getTrigger(): Observable<any> {
    return this.subject.asObservable();
  }
}
