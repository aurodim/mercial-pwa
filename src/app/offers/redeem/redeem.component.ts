import { Component, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { Offer } from '../offer/offer.model';
import { DomSanitizer } from '@angular/platform-browser';
import { Socket } from 'ngx-socket-io';
import { AuthService } from 'src/app/auth/auth.service';
import { APIService } from 'src/app/auth/api.service';
import { OfferViewerService } from './viewer.offer.service';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-redeem-viewer',
  templateUrl: './redeem.component.html'
})
export class RedeemComponent implements AfterViewInit {
  @Input() offer: Offer;
  @Output() hideViewer: EventEmitter<boolean> = new EventEmitter<boolean>();
  redeemed = false;
  stars: object = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  currentStar: number;
  zipcodeNeeded = false;
  zipcodeEntered = false;
  rated = false;
  selectedLocation: string = null;
  rid: string = null;
  merchipsNeeded: number;
  showCode = false;
  fastRedeemCode = null;

  constructor(private sanitizer: DomSanitizer, private socket: Socket, private toastService: MzToastService,
              private authService: AuthService, private api: APIService, private offerViewerService: OfferViewerService) {

  }

  ngAfterViewInit() {
    if (!this.offer) {
      return this.onHide();
    }
    this.zipcodeNeeded = this.offer.locations.length > 1;
    this.merchipsNeeded = this.offer.merchips - (this.authService.getMerchips() as number);
    this.listen();
  }

  listen() {
    this.socket.on(`${this.authService.getSocket()}-${this.offer.id}.redeemed`, (data: { _s: any; _e: { _z: boolean; _m: number; }; }) => {
      if (data._s) {
        this.redeemed = true;
        this.zipcodeNeeded = data._e._z;
        this.authService.setMerchips(data._e._m);
      }
    });
  }

  onLocationSelect(locationSelector: any) {
    this.selectedLocation = this.offer.locations[locationSelector.selectedIndex - 1].formatted;
    this.api.enterZipcode({id : this.offer.id, rid : this.rid, location : this.offer.locations[locationSelector.selectedIndex - 1],
       auth : this.authService.getAUTH()},
      (response: any) => {
        const res = response._body._d;
        if (!res.z) {
          this.toastService.show('Invalid location', 3000, 'darkgrey');
          return;
        }
        this.zipcodeEntered = true;
      }
    );
  }

  onRated() {
    this.api.rateFranchise({id : this.offer.id, rating : this.currentStar, auth : this.authService.getAUTH()},
      (response: any) => {
        const res = response._body._d;
        if (!res.r) {
          this.toastService.show('Invalid rating', 3000, 'darkgrey');
          return;
        }
        this.rated = true;
      }
    );
  }

  onGetFastQR() {
    this.api.fastQR({id: this.offer.id, auth: this.authService.getAUTH()},
    (response: any) => {
      this.fastRedeemCode = this.sanitizer.bypassSecurityTrustHtml(response);
    });
  }

  onHide() {
    this.hideViewer.emit(true);
    this.offerViewerService.triggerViewer(false);
  }
}
