import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { Offer } from './offer/offer.model';
import { OfferService } from './offers.service';
import { OfferViewerService } from './redeem/viewer.offer.service';
import { ToolbarService } from '../toolbar.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  offers: Offer[] = [];
  keepLoading = true;
  offerAvailable = false;
  currentOffer: Offer = null;
  userZipcode: any;
  filterOpen = false;

  constructor(authService: AuthService, router: Router, private offerService: OfferService,
              private offerViewerService: OfferViewerService, private toolbarService: ToolbarService) {
    if (!authService.isAuthenticated()) {
      router.navigate(['login']);
    }
  }

  ngOnInit() {
    this.toolbarService.getFilterOffers().subscribe((open) => {
      if (open) {
        this.filterOpen = true;
      } else {
        this.onFilterClose();
      }
    });
    this.toolbarService.load();
    this.offerService.requestOffers(() => {
      this.offers = this.offerService.getOffers();
      this.offerService.previousPopulations = 1;
      this.userZipcode = this.offerService.getZipcode();
      this.toolbarService.stopLoading();
    }, () => {
      this.toolbarService.stopLoading();
    });
  }

  showOffer($event: Offer) {
    this.currentOffer = $event;
    this.offerAvailable = true;
    this.offerViewerService.triggerViewer(true);
  }

  clearOffer() {
    this.offerAvailable = false;
    this.currentOffer = null;
  }

  onFilterChange() {
    this.offers = [];
    this.offerService.previousPopulations = 1;
    this.toolbarService.load();
    this.offerService.requestOffers(() => {
      this.offers = this.offerService.getOffers();
      this.keepLoading = true;
      this.onFilterClose();
      this.toolbarService.stopLoading();
    }, () => {
      this.toolbarService.stopLoading();
    });
  }

  onFilterClose($event?: { target: { className: string; }; }) {
    if ($event) {
      if ($event.target.className !== 'filter-viewer-backdrop') {
        return;
      }
    }
    setTimeout(() => {
      this.filterOpen = false;
    }, 50);
    this.toolbarService.filterOffersClosed();
  }

  onAttribSelect($event: { target: { value: string; }; }) {
    if ($event.target.value === 'none') {
      this.offerService.filters.meta = null;
      return;
    }
    this.offerService.filters.meta = $event.target.value;
  }

  onDateSelect($event: { target: { value: any; }; }) {
    this.offerService.filters.date = $event.target.value;
  }

  onDistanceSelect($event: { target: { value: any; }; }) {
    this.offerService.filters.location = $event.target.value;
  }

  populateOlderFeed(): Promise<any> {
    return new Promise((resolve) => {
      this.offerService.olderFeed((olderFeed: Offer[]) => {
        if (olderFeed.length === 0) {
          this.keepLoading = false;
        }
        this.offers = [...this.offers, ...olderFeed];
        resolve();
      }, () => resolve());
    });
  }
}
