import { Injectable } from '@angular/core';
import { Offer } from './offer/offer.model';
import { APIService } from '../auth/api.service';

@Injectable()
export class OfferService {
  private offers: Offer[] = [];
  private userZipcode: any;
  public previousPopulations = 1;
  public filters: any = {
    date : 'newest',
    meta : null,
    location : 'five'
  };

  constructor(private api: APIService) {}

  requestOffers(callback: () => void, onError?: () => void) {
    this.api.offersData(0, this.filters,
      (response: any) => {
        this.offers = [];
        const res = response._body._d;
        this.userZipcode = res._z;
        for (const offer of res._o) {
          this.offers.push(new Offer(offer.id, offer.franchise, offer.name, offer.image, offer.locations, offer.website,
             offer.redeem_code, offer.redeem_verify_code, offer.redeemable, offer.merchips, offer.savings, offer.description));
        }
        callback();
      }, onError
    );
  }

  olderFeed(callback: (arr: Offer[]) => void, onError?: () => void) {
    this.api.offersData(this.previousPopulations, this.filters,
      (response: any) => {
        const res = response._body._d;
        const prevArr: Offer[] = [];
        for (const offer of res._o) {
          prevArr.push(new Offer(offer.id, offer.franchise, offer.name, offer.image, offer.locations, offer.website,
             offer.redeem_code, offer.redeem_verify_code, offer.redeemable, offer.merchips, offer.savings, offer.description));
        }
        this.previousPopulations++;
        callback(prevArr);
      }, onError
    );
  }

  getOffers() {
    return this.offers.slice();
  }

  public getZipcode() {
    return this.userZipcode;
  }

  getOffer(id: string) {
    return this.offers.find(offer => offer.id === id);
  }
}
