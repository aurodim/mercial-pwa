import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Offer } from './offer.model';
import { APIService } from 'src/app/auth/api.service';
import { AuthService } from 'src/app/auth/auth.service';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-mercial-offer',
  templateUrl: './offer.component.html',
  styles: [
    `
      .addresses {
        z-index : 10000;
      }
    `
  ]
})
export class OfferComponent implements OnInit {
  @Input() offer: Offer;
  @Input() userLat: string;
  @Input() userLng: string;
  @Output() selected: EventEmitter<Offer> = new EventEmitter<Offer>();
  reportReason = '';
  reportExpand = '';
  hideRedeem = true;
  reasons = [
    { value : 'unexistent', formatted : 'Unexistent - The franchise owner is not a member' },
    { value : 'fake', formatted : 'Fake - Franchise owner scanned my code but did not give me the offer' },
  ];
  translationStrings: string[] = [];
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: () => { // Callback for Modal open. Modal and trigger parameters available.

    },
    complete: () => { } // Callback for Modal close
  };

  constructor(private authService: AuthService, private api: APIService, private toastService: MzToastService) {}

  ngOnInit() {}

  onReportSubmit() {
   if (this.reportReason === '') {
     return;
   }

   const report = {
     auth : this.authService.getAUTH(),
     id : this.offer.id,
     type : 'offer',
     reason : this.reportReason,
     expand : this.reportExpand
   };

   this.api.report(report,
     (response: any) => {
       const res = response._body;

       if (res._m === '_invalidreason') {
         this.toastService.show('Invalid reason', 3000, 'red');
         return;
       }

       if (res._m === '_invalidid') {
         this.toastService.show('Video or Offer has been deleted and is no longer available', 3000, 'red');
         return;
       }

       this.toastService.show('Report submitted', 3000, 'darkgrey');
     }
   );
  }

  onNavigation() {
    window.open(`https://www.google.com/maps/search/?api=1&query=${encodeURI(this.offer.locations[0].formatted)}`, '_system');
  }

  encode(url: string) {
    return encodeURI(url);
  }

  onView() {
    this.selected.emit(this.offer);
  }
}
