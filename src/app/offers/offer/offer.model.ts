export class Offer {
  id: string;
  franchise: any;
  name: string;
  description?: string;
  image: string;
  locations: any[];
  website: string;
  redeemCode: string;
  redeemVerifyCode: string;
  redeemable: boolean;
  merchips: number;
  savings: string;
  hiddenCode: string;

  constructor(id: string, franchise: any,  name: string, image: string, locations: Array<{formatted: string, lat: number, lng: number}>,
              website: string, redeemCode: string, redeemVerifyCode: string, redeemable: boolean, merchips: number, savings: string,
              description?: string) {
    this.id = id;
    this.franchise = franchise;
    this.name = name;
    this.description = description;
    this.image = image;
    this.locations = locations;
    this.website = website;
    this.redeemCode = redeemCode;
    this.redeemVerifyCode = redeemVerifyCode;
    this.redeemable = redeemable;
    this.merchips = merchips;
    this.savings = savings;
    this.hiddenCode = this.generateHidden();
  }

  generateHidden() {
    let code = '';
    for (const {} of this.redeemVerifyCode) {
      code += '&bull;';
    }
    return code;
  }

  public getLocations() {
    return this.locations.slice().map(location => location.formatted).join(', ');
  }

  public getCloudinaryID() {
    return this.redeemCode.match(/upload\/v[0-9]*\/(.*)\.(svg|png)/)[1];
  }
}
