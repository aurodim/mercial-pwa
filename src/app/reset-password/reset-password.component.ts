import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { APIService } from '../auth/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['../app.component.scss']
})
export class ResetPasswordComponent {
  resetForm = new FormGroup({
    code : new FormControl('', { validators: [Validators.required]}),
    email : new FormControl('', { validators: [Validators.email, Validators.required]}),
    'new-password' : new FormControl('', { validators: [Validators.required]}),
    'confirm-password' : new FormControl('', { validators: [Validators.required]})
  });

  constructor(authService: AuthService, private toastService: MzToastService, router: Router, private api: APIService) {
    if (authService.isAuthenticated()) {
      router.navigate(['feed']);
    }
  }

  mismatch() {
    if (this.resetForm.controls['new-password'].value !== this.resetForm.controls['confirm-password'].value) {
      this.resetForm.controls['confirm-password'].setErrors({mismatch : true});
    }
  }

  onSubmit() {
    if (this.resetForm.invalid) {
      return;
    }
    this.api.reset(this.resetForm.value,
      (response: any) => {
        if (response._body._m === '_passshort') {
          this.resetForm.controls['new-password'].setErrors({invalid : true});
          this.toastService.show('Date of birth is invalid', 3000, 'red');
        } else if (response._body._m === '_invalidcode') {
          this.resetForm.controls.email.setErrors({invalid : true});
          this.resetForm.controls.code.setErrors({invalid : true});
          this.toastService.show('Invalid code', 3000, 'red');
        } else {
          this.resetForm.reset();
        }
      });
  }
}
