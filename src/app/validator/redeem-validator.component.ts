import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { APIService } from '../auth/api.service';
import { MzModalComponent, MzToastService } from 'ngx-materialize';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-mercial-redeem-validator',
  templateUrl: './redeem-validator.component.html'
})
export class RedeemValidatorComponent implements OnInit, OnDestroy {
  @ViewChild('confirmationModal') confirmationModal: MzModalComponent;
  @ViewChild('handledModal') handledModal: MzModalComponent;
  @ViewChild('successModal') successModal: MzModalComponent;
  awaiting = false;
  success = false;
  codeData: any;
  timeout: any;
  subscription: Subscription;
  confirm: any;
  sc: number;
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: () => { // Callback for Modal open. Modal and trigger parameters available.
    },
    complete: () => {} // Callback for Modal close
  };

  constructor(private socket: Socket, private authService: AuthService, private api: APIService, private toastService: MzToastService) {}

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.sc = Math.floor(Math.random() * 10000000000);
    this.socket.on(this.authService.getSocket(), (data: any) => {
      this.awaiting = true;
      this.codeData = data;
      this.codeData._u = {
        _a : this.authService.getAUTH(),
        _s : this.authService.getSocket() + '-handled',
        _d : null,
        _sc : this.sc
      };

      this.confirmationModal.openModal();
    });
    this.socket.on(this.authService.getSocket() + '-handled', (data: { _e: { _sc: number; }; }) => {
      if (this.confirm) {
        this.confirm.dismiss();
      }
      if (data._e._sc !== this.sc) {
        this.handledModal.openModal();
      }
    });
  }

  onCancel() {
    this.confirm = null;
    this.codeData._u._d = false;
    this.api.validateRedeem({auth : this.authService.getAUTH(), codeData : this.codeData},
      () => {
        this.awaiting = false;
      }
    );
  }

  onApprove() {
    this.confirm = null;
    this.codeData._u._d = true;
    this.api.validateRedeem({auth : this.authService.getAUTH(), codeData : this.codeData},
      (response: any) => {
        const res = response._body._d;
        if (res._s) {
          this.authService.setMerchips(res._m);
          this.awaiting = false;
          this.successModal.openModal();
        } else {
          if (response._body._m === '_notenoughmerchips') {
            this.toastService.show('Not enough merchips', 3000, 'red');
          } else {
            this.toastService.show('Try again', 3000, 'red');
          }
          this.awaiting = false;
          this.success = false;
        }
      }
    );
  }

  onError() {
    this.toastService.show('Try again', 3000, 'red');
    this.awaiting = false;
  }
}
