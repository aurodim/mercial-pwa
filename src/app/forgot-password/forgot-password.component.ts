import { Component } from '@angular/core';
import { APIService } from '../auth/api.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['../app.component.scss']
})
export class ForgotPasswordComponent {
  public datePickerOptions: Pickadate.DateOptions = {
    clear: 'Clear', // Clear button text
    close: 'Ok',    // Ok button text
    today: null, // Today button text
    closeOnClear: false,
    closeOnSelect: true,
    format: 'mmmm dd, yyyy', // Visible date format (defaulted to formatSubmit if provided otherwise 'd mmmm, yyyy')
    formatSubmit: 'yyyy-mm-dd',   // Return value format (used to set/get value)
    onClose: () => {
      const datePickers = document.getElementsByClassName('picker__holder');
      for (const picker of datePickers as unknown as any[]) {
        picker.style.pointerEvents = 'none';
      }
    },
    onOpen: () => {},
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 100,    // Creates a dropdown of 10 years to control year,
    max: null,
    min: new Date('1910/1/1')
  };

  constructor(private api: APIService, private toastService: MzToastService, authService: AuthService, private router: Router) {
    if (authService.isAuthenticated()) {
      router.navigate(['feed']);
    }
    this.datePickerOptions.max = new Date(new Date().getTime() - 4.1e+11);
  }

  onDateChoose() {
    const datePickers = document.getElementsByClassName('picker__holder');
    for (const picker of datePickers as unknown as any[]) {
      picker.style.pointerEvents = 'all';
    }
  }

  onSubmit(forgotForm: NgForm) {
    if (forgotForm.invalid) {
      return;
    }
    this.api.forgot(forgotForm.value,
      (response: any) => {
        if (response._body._m === '_mismatch') {
          forgotForm.controls.email.setErrors({invalid : true});
          forgotForm.controls.birthdate.setErrors({invalid : true});
          this.toastService.show('Date of birth and E-mail do not match our records', 3000, 'red');
          return;
        } else {
          forgotForm.resetForm();
        }
        this.router.navigate(['/reset']);
      });
  }
}
