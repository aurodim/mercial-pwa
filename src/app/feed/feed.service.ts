import { Injectable } from '@angular/core';
import { Mercial } from './mercial/mercial.model';
import { APIService } from '../auth/api.service';

@Injectable()
export class FeedService {
  private feed: Mercial[] = [];
  public previousPopulations = 1;
  public filters: any = {
    date : 'newest',
    meta : null,
    categories : null
  };

  constructor(private api: APIService) { }

  requestFeed(callback: () => void, onError?: () => void) {
    this.api.feedData(0, this.filters, (response: any) => {
        this.feed = [];
        const res = response._body._d;
        for (const mercial of res._f) {
          this.feed.push(
            new Mercial(mercial.id, this.getPublisher(mercial.publisher),
             mercial.title, mercial.thumbnail, mercial.video, mercial.size,
              mercial.merchips, mercial.duration, mercial.tags, mercial.description,
               mercial.more, mercial.attributes));
        }
        callback();
      }, onError
    );
  }

  olderFeed(callback: (arr: Mercial[]) => void, onError?: () => void) {
    this.api.feedData(this.previousPopulations, this.filters,
      (response: any) => {
        const res = response._body._d;
        const prevArr: Mercial[] = [];
        for (const mercial of res._f) {
          prevArr.push(
            new Mercial(mercial.id, this.getPublisher(mercial.publisher), mercial.title,
             mercial.thumbnail, mercial.video, mercial.size, mercial.merchips, mercial.duration,
              mercial.tags, mercial.description, mercial.more, mercial.attributes));
        }
        this.previousPopulations++;
        callback(prevArr);
      }, onError
    );
  }

  getPublisher(publisher: string) {
    if (!publisher) {
      return null;
    }

    if (publisher.replace(/ /gi, '') === '') {
      return null;
    } else {
      return publisher;
    }
  }

  clearFeed() {
    this.feed = [];
  }

  getFeed() {
    return this.feed.slice();
  }

  getFeedItem(id: string) {
    return this.feed.find(ad => ad.id === id);
  }

  hasProperty(id: string, property: string) {
    const ad: Mercial = this.getFeedItem(id);
    return ad.attributes.indexOf(property) !== -1 ? true : false;
  }

  setFeed(feed: Mercial[]) {
    this.feed = feed;
  }
}
