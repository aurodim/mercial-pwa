import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Mercial } from './mercial.model';
import { AuthService } from 'src/app/auth/auth.service';
import { APIService } from 'src/app/auth/api.service';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-mercial-mercial',
  templateUrl: './mercial.component.html',
  styles: [`
    .tags {
      z-index: 100000;
    }
  `]
})
export class MercialComponent implements OnInit {
  @Input() mercial: Mercial;
  @Output() selected: EventEmitter<Mercial> = new EventEmitter<Mercial>();
  reportReason = '';
  reportExpand = '';
  moreInfoF = [];
  moreInfoU = [];
  reasons = [
      { value : 'disturbing', formatted : 'Disturbing content' },
      { value : 'violent', formatted : 'Violent or repulsive content' },
      { value : 'hateful', formatted : 'Hateful or abusive content' },
      { value : 'terrorism', formatted : 'Promotes terrorism' },
      { value : 'infringement', formatted : 'Infringes my rights' },
      { value : 'restriction', formatted : 'Violates age restriction' },
  ];
  translationStrings: string[] = [];
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: () => { // Callback for Modal open. Modal and trigger parameters available.

    },
    complete: () => { } // Callback for Modal close
  };

  constructor(private api: APIService, private authService: AuthService, private toastService: MzToastService) {

  }

  ngOnInit() {
    const moreInfo = this.setupInfo();
    for (const more in moreInfo) {
      this.moreInfoF.push(more);
      this.moreInfoU.push(moreInfo[more]);
    }
  }

  onView() {
    this.selected.emit(this.mercial);
  }

 onReportSubmit() {
   if (this.reportReason === '') {
     return;
   }

   const report = {
     auth : this.authService.getAUTH(),
     id : this.mercial.id,
     type : 'mercial',
     reason : this.reportReason,
     expand : this.reportExpand
   };

   this.api.report(report, (response: any) => {
       const res = response._body;
       if (res._m === '_invalidreason') {
         this.toastService.show('Invalid reason', 3000, 'red', () => {});
         return;
       }

       if (res._m === '_invalidid') {
         this.toastService.show('Video or Offer has been deleted and is no longer available', 3000, 'red', () => {});
         return;
       }

       this.toastService.show('Report submitted', 3000, 'green', () => {});
   });
  }

  setupInfo() {
   const arr = {};
   if (this.mercial.more) {
       this.mercial.more.forEach(more => {
        let formatted: any = '';
        let after = [];
        let moreCopy = more;
        if (moreCopy.includes('://')) {
          moreCopy = moreCopy.substring(moreCopy.indexOf('://') + 3);
          after = moreCopy.split('/');
          after  = after.map(a => a.toUpperCase());
        }
        formatted = moreCopy.split('/')[0].split('.');
        if (formatted.length === 2) {
          formatted = formatted[0].substring(0, 1).toUpperCase() + formatted[0].substring(1);
        } else {
          formatted = formatted[1].substring(0, 1).toUpperCase() + formatted[1].substring(1);
        }
        if (after.length > 1) {
          formatted += ' - ' + after.splice(1).join(' ');
        }
        arr[formatted] = more;
      });
    }
    return arr;
  }

  onInformation() {
    this.api.addInterest({id : this.mercial.id, auth : this.authService.getAUTH()});
  }

  cleanMore(url: string) {
    if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
      url = 'http://' + url;
    }
    return url;
  }
}
