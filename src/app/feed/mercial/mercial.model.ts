export class Mercial {
  id: string;
  publisher: string;
  title: string;
  thumbnail: string;
  video: string;
  duration: number;
  size: string;
  merchips: number;
  tags?: string[];
  description?: string;
  more?: string[];
  attributes?: any[];
  widthPixel: number;
  heightPixel: number;

  constructor(id: string, publisher: string, title: string, thumbnail: string,
              video: string, size: string, merchips: number, duration: number,
              tags?: string[], description?: string, more?: string[], attributes?: any[]) {
    if (size === 'large') {
      this.widthPixel = 450; this.heightPixel = 350;
    } else if (size === 'medium') {
      this.widthPixel = 350; this.heightPixel = 300;
    } else {
      this.widthPixel = 250; this.heightPixel = 250;
    }

    this.id = id;
    this.publisher = publisher;
    this.title = title;
    this.thumbnail = thumbnail;
    this.video = video;
    this.size = size;
    this.merchips = merchips;
    this.description = description;
    this.tags = tags.map((tag) => {
      let final = '';
      let parts = tag.split('-');
      for (const part of parts) {
        final += part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase() + '-';
      }
      final = final.substring(0, final.length - 1);

      parts = final.split(' ');
      final = '';
      for (const part of parts) {
        final += part.substring(0, 1).toUpperCase() + part.substring(1) + ' ';
      }
      final = final.substring(0, final.length - 1);

      return final;
    });
    this.more = more;
    this.duration = duration;
    this.attributes = attributes;
  }

  public getTags() {
    let toRemove = 0;
    if (this.tags.length > 2) {
      toRemove = this.tags.length - 2;
    }
    const toReturn = this.tags.slice();
    toReturn.splice(2, toRemove);
    return toReturn.join(' / ');
  }

  public Cloudinarize(link: string, width: any, extras?: string, isVideo = false) {
    const toReturn = link.replace('/upload/', `/upload/w_${width}${extras ? ',' + extras : ''}/`).split('.');
    if (!isVideo) {
      return toReturn.join('.');
    }
    toReturn.pop();
    return toReturn.join('.') + '.mp4';
  }

  public getSigns() {
    if (this.size === 'small') {
      return '$';
    } else if (this.size === 'medium') {
      return '$$';
    } else {
      return '$$$';
    }
  }
}
