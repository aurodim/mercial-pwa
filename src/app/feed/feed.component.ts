import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Mercial } from './mercial/mercial.model';
import { FeedService } from './feed.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { MercialViewerService } from './viewer/viewer.mercial.service';
import { ToolbarService } from '../toolbar.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, AfterViewInit {
  ads: Mercial[] = [];
  mercialAvailable = false;
  currentMercial: Mercial = null;
  keepLoading = true;
  filterOpen = false;
  categories = {
    comedy: 'Comedy',
    beauty: 'Beauty',
    music: 'Music',
    sports: 'Sports',
    tv: 'TV',
    tech: 'Tech',
    politics: 'Politics',
    automobiles: 'Automobiles',
    products: 'Products'
  };
  categoriesKeys = [];

  constructor(private feedService: FeedService, private authService: AuthService, router: Router,
              private mercialViewerService: MercialViewerService,
              private toolbarService: ToolbarService) {
                if (!authService.isAuthenticated()) {
                  router.navigate(['login']);
                }
                this.categoriesKeys = Object.keys(this.categories);
  }

  ngOnInit() {
    this.mercialViewerService.getTrigger().subscribe((show) => {
      if (!show) {
        this.clearMercial();
      }
    });
    this.toolbarService.getRefresh().subscribe(() => {
      this.onRefresh();
    });
    this.toolbarService.getFilter().subscribe((open) => {
      if (open) {
        this.filterOpen = true;
      } else {
        this.onFilterClose();
      }
    });
  }

  ngAfterViewInit() {
    if (!this.authService.getAUTH()) {
      return;
    }
    this.toolbarService.load();
    this.feedService.requestFeed(() => {
      this.feedService.previousPopulations = 1;
      this.ads = [];
      this.ads = this.feedService.getFeed();
      this.toolbarService.stopLoading();
    });
  }

  onRefresh() {
    this.ads = [];
    this.feedService.clearFeed();
    this.toolbarService.load();
    this.feedService.requestFeed(() => {
      this.ads = [];
      this.ads = this.feedService.getFeed();
      this.feedService.previousPopulations = 1;
      this.toolbarService.stopLoading();
    });
    this.keepLoading = true;
  }

  onFilterChange() {
    this.feedService.previousPopulations = 1;
    this.toolbarService.load();
    this.feedService.requestFeed(() => {
      this.ads = [];
      this.ads = this.feedService.getFeed();
      this.toolbarService.stopLoading();
    }, () => {
      this.toolbarService.stopLoading();
    });
    this.keepLoading = true;
    this.onFilterClose();
  }

  onFilterClose($event?: { target: { className: string; }; }) {
    if ($event) {
      if ($event.target.className !== 'filter-viewer-backdrop') {
        return;
      }
    }
    setTimeout(() => {
      this.filterOpen = false;
    }, 300);
    this.toolbarService.filterClosed();
  }

  showMercial($event: Mercial) {
    this.currentMercial = $event;
    this.mercialAvailable = true;
    this.mercialViewerService.triggerVideo(true);
  }

  public clearMercial() {
    this.mercialAvailable = false;
    this.currentMercial = null;
  }

  onCategorySelect(e: { target: { value: string; }; }) {
    if (this.feedService.filters.categories &&
       this.feedService.filters.categories.includes(e.target.value)) {
      const arr = this.feedService.filters.categories.split(',');
      arr.splice(arr.indexOf(e.target.value), 1);
      this.feedService.filters.categories = arr.join(',');
      if (this.feedService.filters.categories.length === 0) {
        this.feedService.filters.categories = null;
      }
    } else {
      if (this.feedService.filters.categories) {
        this.feedService.filters.categories += ',' + e.target.value;
      } else {
        this.feedService.filters.categories = e.target.value;
      }
    }
  }

  onAttribSelect($event: { target: { value: string; }; }) {
    if ($event.target.value === 'none') {
      this.feedService.filters.meta = null;
      return;
    }
    this.feedService.filters.meta = $event.target.value;
  }

  onDateSelect($event: { target: { value: any; }; }) {
    this.feedService.filters.date = $event.target.value;
  }

  populateOlderFeed(): Promise<any> {
    if (!this.keepLoading) {
      return;
    }
    return new Promise((resolve) => {
      this.feedService.olderFeed((olderFeed: Mercial[]) => {
        if (olderFeed.length === 0) {
          this.keepLoading = false;
        }
        this.ads = [...this.ads, ...olderFeed];
        resolve();
      }, () => resolve());
    });
  }
}
