import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class MercialViewerService {
  private subject = new Subject<boolean>();
  private videoShowing = false;

  triggerVideo(show: boolean) {
    this.videoShowing = show;
    this.subject.next(show);
  }

  getTrigger(): Observable<any> {
    return this.subject.asObservable();
  }

  getShowing() {
    return this.videoShowing;
  }
}
