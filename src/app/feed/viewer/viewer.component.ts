import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ViewChild, HostListener } from '@angular/core';
import { Mercial } from '../mercial/mercial.model';
import { MercialViewerService } from './viewer.mercial.service';
import { APIService } from 'src/app/auth/api.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector : 'app-feed-viewer',
  templateUrl: './viewer.component.html'
})
export class ViewerComponent implements OnInit, AfterViewInit {
  @Input() mercial: Mercial;
  @Output() hideViewer: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('viewerPlayer') viewerPlayer: any;
  width: number;
  ad: Mercial;
  cancel = 'Cancel';
  showControls = false;
  loadFailed = false;
  seconds = 0;
  left = 0;
  ended = false;
  watching = true;
  blurred = true;
  seeking = false;
  timeout;
  hideable = false;
  awardedMerchips = 0;
  interval;

  @HostListener('window:blur', ['$event'])
    onBlur(): void {
      if (!this.loadFailed && !this.ended) {
        if (!this.viewerPlayer) {
          return;
        }
        this.onPause();
      }
    }

  constructor(private authService: AuthService, private api: APIService, private mercialViewerService: MercialViewerService) {}

  ngOnInit() {
    this.width = window.innerWidth - 20;
    this.ad = this.mercial;
  }

  ngAfterViewInit() {}

  onLoad() {
    if (document.getElementById('vh')) {
      document.getElementById('vh').style.height =
      document.getElementById('v').clientHeight > 300 ? document.getElementById('v').clientHeight + 'px' : '300px';
    }
    this.playVideo();
  }

  hasProperty(property: string) {
    if (!this.ad) {
      return false;
    }
    return this.ad.attributes.indexOf(property) !== -1 ? true : false;
  }

  formatTime(time: number) {
    const format = ['0', '0', ':', '0', '0'];
    if (time > 60) {
      format[0] = Math.floor((time / 60 / 10)).toFixed(0);
      format[1] = Math.floor(((time / 60) % 10)).toFixed(0);
    }
    format[3] = Math.floor((time % 60 / 10)).toFixed(0);
    format[4] = Math.floor(((time % 60) % 10)).toFixed(0);
    return format.join('');
  }


  onPause() {
    this.showControls = true;
    this.blurred = true;
    this.watching = false;
    if (!this.viewerPlayer) {
      return;
    }
    this.viewerPlayer.nativeElement.pause();
    if (document.getElementById('tks')) {
      document.getElementById('tks').style.animationPlayState = 'paused';
    }
  }

  onVideoOptions() {
    this.showControls = true;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (this.showControls && !this.blurred) {
        this.showControls = false;
      }
    }, 2000);
  }

  playVideo() {
    if (this.loadFailed) {
      this.onError();
      return;
    }
    this.blurred = false;
    if (!this.viewerPlayer) {
      return;
    }
    if (!this.timeout) {
      this.showControls = false;
    }
    this.viewerPlayer.nativeElement.play();
    if (document.getElementById('tks')) {
      document.getElementById('tks').style.animationPlayState = 'running';
      document.getElementById('tks').style.animationDuration = Math.ceil(this.ad.duration) + 's';
    }
    this.watching = true;
    this.interval = setInterval(() => {
      if (!this.viewerPlayer) {
        return;
      }
      this.seconds = this.viewerPlayer.nativeElement.currentTime;
      this.left = Math.ceil(this.ad.duration) - (this.seconds + 0.3);
    }, 1000);
  }

  onSeeking() {
    if (!this.viewerPlayer) {
      return;
    }
    if (this.seconds < this.viewerPlayer.nativeElement.currentTime) {
      this.viewerPlayer.nativeElement.currentTime = this.seconds;
    }
  }

  onSeeked() {
    if (!this.viewerPlayer) {
      return;
    }
    if (this.seconds < this.viewerPlayer.nativeElement.currentTime) {
      this.viewerPlayer.nativeElement.currentTime = this.seconds;
    }
  }

  onTimeChange() {
    if (!this.viewerPlayer) {
      return;
    }
    if (this.blurred) {
      this.viewerPlayer.nativeElement.pause();
      return;
    }
    this.viewerPlayer.nativeElement.playbackRate = 1;
  }

  onEnded() {
    this.api.mercialWatched({id : this.mercial.id, auth : this.authService.getAUTH()},
      (response: any) => {
        const res = response._body._d;
        this.awardedMerchips = res._mr;
        this.authService.setMerchips(res._ma);
        this.ended = true;
      }, () => {}
    );
  }

  onHide() {
    if (this.viewerPlayer) {
      this.viewerPlayer.nativeElement.pause();
      this.viewerPlayer.nativeElement.src = '';
    }
    clearInterval(this.interval);
    this.hideViewer.emit(true);
    this.mercialViewerService.triggerVideo(false);
  }

  onError() {
    this.loadFailed = true;
  }

  onInformation() {
    if (this.mercial.more) {
      this.mercial.more.forEach(more => {
       let formatted: any = '';
       let after = [];
       let moreCopy = more;
       if (moreCopy.includes('://')) {
         moreCopy = moreCopy.substring(moreCopy.indexOf('://') + 3);
         after = moreCopy.split('/');
         after  = after.map(a => a.toUpperCase());
       }
       formatted = moreCopy.split('/')[0].split('.');
       if (formatted.length === 2) {
         formatted = formatted[0].substring(0, 1).toUpperCase() + formatted[0].substring(1);
       } else {
         formatted = formatted[1].substring(0, 1).toUpperCase() + formatted[1].substring(1);
       }
       if (after.length > 1) {
         formatted += ' - ' + after.splice(1).join(' ');
       }
      });
    }
 }

 cleanMore(url: string) {
   if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
     url = 'http://' + url;
   }
   return url;
 }
}
