import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { APIService } from '../auth/api.service';
import { NgForm } from '@angular/forms';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../app.component.scss']
})
export class RegisterComponent {
  public datePickerOptions: Pickadate.DateOptions = {
    clear: 'Clear', // Clear button text
    close: 'Ok',    // Ok button text
    today: null, // Today button text
    closeOnClear: true,
    closeOnSelect: false,
    format: 'mmmm dd, yyyy', // Visible date format (defaulted to formatSubmit if provided otherwise 'd mmmm, yyyy')
    formatSubmit: 'yyyy-mm-dd',   // Return value format (used to set/get value)
    onClose: () => {
      const datePickers = document.getElementsByClassName('picker__holder');
      for (const picker of datePickers as unknown as any[]) {
        picker.style.pointerEvents = 'none';
      }
    },
    onOpen: () => {},
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 100,    // Creates a dropdown of 10 years to control year,
    max: null,
    min: new Date('1910/1/1')
  };

  constructor(private authService: AuthService, private toastService: MzToastService, private router: Router, private api: APIService) {
    if (authService.isAuthenticated()) {
      router.navigate(['feed']);
    }
    this.datePickerOptions.max = new Date(new Date().getTime() - 4.1e+11);
  }

  onDateChoose() {
    const datePickers = document.getElementsByClassName('picker__holder');
    for (const picker of datePickers as unknown as any[]) {
      picker.style.pointerEvents = 'all';
    }
  }

  onRegisterSubmit(registerForm: NgForm) {
    if (registerForm.invalid) {
      return;
    }
    this.api.registerUser(registerForm.value,
      (response: any) => {
        if (response._body._m === '_emailinuse') {
          registerForm.controls.email.setErrors({inuse : true});
          this.toastService.show('E-mail already in use', 3000, 'red');
        } else if (response._body._m === '_dobinvalid') {
          registerForm.controls.dob.setErrors({invalid : true});
          this.toastService.show('Date of birth is invalid', 3000, 'red');
        } else if (response._body._m === '_passshort') {
          registerForm.controls.password.setErrors({invalid : true});
          this.toastService.show('Password must be at least 4 characters long', 3000, 'red');
        } else {
          this.toastService.show('Success!', 3000, 'darkgrey');
          this.authService.authenticate(response._body, () => {
            this.router.navigate(['feed']);
          });
          registerForm.resetForm();
        }
      });
  }
}
