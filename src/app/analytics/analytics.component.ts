import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { APIService } from '../auth/api.service';
import { ToolbarService } from '../toolbar.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  pieChartLabels: string[] = [];
  pieChartData: number[] = [];
  pieChartType = 'pie';
  pieChartColors: Array < any > = [{
   backgroundColor: [],
   borderColor: []
  }];

  monthlyData: Array<any> = [
    {data: [], label : 'Offers Redeemed', hidden : true},
    {data: [], label : 'Mercials Watched', hidden : true},
    {data: [], label : 'Both'}
  ];
  secondsMonthlyData: Array<any> = [
    {data: []}
  ];
  lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June',
   'July', 'August', 'September', 'October', 'November', 'December'];
  lineChartOptions: any = {
    responsive: true,
    scales: {
        yAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                beginAtZero: true   // minimum value will be 0.
            }
        }]
    },
        // Container for pan options
    pan: {
      // Boolean to enable panning
      enabled: true,

      // Panning directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow panning in the y direction
      mode: 'y',
      rangeMin: {
        // Format of min pan range depends on scale type
        x: null,
        y: 0
      },
      rangeMax: {
        // Format of max pan range depends on scale type
        x: null,
        y: null
      }
    },

    // Container for zoom options
    zoom: {
      // Boolean to enable zooming
      enabled: true,

      // Enable drag-to-zoom behavior
      drag: false,

      // Zooming directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow zooming in the y direction
      mode: 'y',
      rangeMin: {
        // Format of min zoom range depends on scale type
        x: null,
        y: 0
      },
      rangeMax: {
        // Format of max zoom range depends on scale type
        x: null,
        y: null
      }
    }
  };
  lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(102,51,153,0.2)',
      borderColor: 'rgb(102,51,153)',
      pointBackgroundColor: 'rgb(102,51,153)',
      pointBorderColor: 'rgb(102,51,153)',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(102,51,153)'
    }
  ];
  monthlyDataColors: Array<any> = [];
  public lineChartLegend = false;
  public lineChartType = 'line';
  reviewsAnswered: number;
  mercialsWatched: number;
  merchipsEarned: number;
  moneySaved: number;
  offersRedeemed: number;

  constructor(private authService: AuthService, router: Router, private api: APIService, private toolbarService: ToolbarService) {
    if (!authService.isAuthenticated()) {
      router.navigate(['login']);
    }
  }

  ngOnInit() {
    this.toolbarService.load();
    this.api.analyticsData(
      (response: any) => {
        const res = response._body._d;
        this.merchipsEarned = res._me;
        this.moneySaved = res._ms;
        this.reviewsAnswered = res._ra;
        this.mercialsWatched = res._mw;
        this.offersRedeemed = res._or;
        this.pieChartLabels = res._z.labels;
        this.pieChartData = res._z.data;
        this.pieChartColors[0].backgroundColor = res._z.colors.fill;
        this.pieChartColors[0].borderColor = res._z.colors.borders;
        this.monthlyData[0].data = res._md._or;
        this.monthlyData[1].data = res._md._mw;
        this.monthlyData[2].data = res._md._mo;
        this.monthlyDataColors = res._md.colors;
        this.secondsMonthlyData[0].data = res._smd;
        this.authService.setMerchips(res._mm);
        this.toolbarService.stopLoading();
      }, () => {
        this.toolbarService.stopLoading();
      }
    );
  }
}
