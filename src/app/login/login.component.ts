import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { APIService } from '../auth/api.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private authService: AuthService, private toastService: MzToastService, private api: APIService, private router: Router) {
    if (authService.isAuthenticated()) {
      router.navigate(['feed']);
    }
  }
  onLogin(loginForm: NgForm) {
    if (loginForm.invalid) {
      return;
    }
    this.api.loginUser(loginForm.value,
      (response: any) => {
        if (response._body._m === '_emailinvalid') {
          loginForm.controls.email.setErrors({invalid : true});
          this.toastService.show('E-mail not recognized', 3000, 'red');
        } else if (response._body._m === '_passinvalid') {
          loginForm.controls.password.setErrors({invalid : true});
          this.toastService.show('Invalid password', 3000, 'red');
        } else if (response._body._m === '_accountlocked') {
          loginForm.controls.email.setErrors({invalid : true});
          loginForm.controls.password.setErrors({invalid : true});
          this.toastService.show('Account has been locked. Contact support.', 3000, 'red');
        } else {
          this.authService.authenticate(response._body, () => {
            this.router.navigate(['feed']);
          });
          loginForm.resetForm();
          return;
        }
      }
    );
  }
}
