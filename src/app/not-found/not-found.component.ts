import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {
  path = '';

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.path = unescape(window.location.pathname);
  }

}
